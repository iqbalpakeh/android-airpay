package com.visa.airpay.payment;

import java.util.concurrent.LinkedBlockingDeque;

public class CvmFlag {

    /**
     * The only acceptable value for this flag
     */
    public static String FLAG = "TRUE";

    /**
     * Reference to singletone object
     */
    private static CvmFlag mFlag;

    /**
     * Dequeue object used to wait thread
     */
    private LinkedBlockingDeque<String> mBlockingDeque;

    /**
     * To retrieve singletone object from CvmFlag
     *
     * @return CvmFlag object
     */
    public static synchronized CvmFlag getInstance() {
        if (mFlag == null) mFlag = new CvmFlag();
        return mFlag;
    }

    private CvmFlag() {
        mBlockingDeque = new LinkedBlockingDeque<>();
    }

    /**
     * Clear flag
     */
    public synchronized void destroyFlag() {
        mFlag = null;
    }

    /**
     * Put value on the dequeue to start process
     *
     * @param flag of CvmFlag
     */
    public void putFlag(String flag) {
        try {
            mBlockingDeque.put(flag);
        } catch (InterruptedException error) {
            error.printStackTrace();
        }
    }

    /**
     * Retrieve flag from CvmFlag. Stop thread of buffer is empty.
     *
     * @return CvmFlag object
     */
    public String takeFlag() {
        String flag = "";
        try {
            flag = mBlockingDeque.take();
        } catch (InterruptedException error) {
            error.printStackTrace();
        }
        return flag;
    }

}

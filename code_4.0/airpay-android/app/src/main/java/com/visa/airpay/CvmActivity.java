package com.visa.airpay;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.visa.airpay.payment.CvmFlag;
import com.visa.airpay.transaction.Trx;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class CvmActivity extends AppCompatActivity {

    private static String TAG = "CvmActivity";

    private String mAmount;

    private String mMerchant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cvm);

        mAmount = getIntent().getStringExtra("EXTRA_AMOUNT");
        mMerchant = getIntent().getStringExtra("EXTRA_MERCHANT");
        Log.d(TAG, "amount = " + mAmount + ", merchant = " + mMerchant);

        TextView amount = findViewById(R.id.trx_amount);
        amount.setText(new DecimalFormat("#0.00").format(new BigDecimal(mAmount)));

        TextView merchant = findViewById(R.id.trx_merchant);
        merchant.setText(mMerchant);

        ImageView fingerPrint = findViewById(R.id.finger_print);
        fingerPrint.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String timestamp = String.valueOf(System.currentTimeMillis());
                String amount = mAmount;
                String merchant = mMerchant;
                Trx.build(timestamp, amount, merchant).store(CvmActivity.this);
                NavUtils.navigateUpFromSameTask(CvmActivity.this);
                CvmFlag.getInstance().putFlag(CvmFlag.FLAG);
                return true;
            }
        });

    }

}

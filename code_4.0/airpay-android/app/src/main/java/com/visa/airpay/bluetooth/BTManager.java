package com.visa.airpay.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.visa.airpay.payment.AirpayApplet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.UUID;

public class BTManager {

    /**
     * Used for debugging
     */
    private static final String TAG = "BTManager";

    /**
     * Airpay server UUID
     */
    private static final UUID AIRPAY_UUID = UUID.fromString("04c6093b-0000-1000-8000-00805f9b34fd");

    /**
     * Singletone reference object
     */
    private static BTManager mBTManager;

    /**
     * Reference to remote bluetooth device that have airpay server (Desktop Application)
     */
    private BluetoothDevice mBTDevice;

    /**
     * Bluetooth socket reference
     */
    private BluetoothSocket mSocket;

    /**
     * Singletone function to retrieve BTManager object
     *
     * @return BTManager object
     */
    public static synchronized BTManager getInstance() {
        if (mBTManager == null) mBTManager = new BTManager();
        return mBTManager;
    }

    /**
     * Pair Remote bluetooth device (POS) to BTManager
     *
     * @param device remote bluetooth device
     */
    public void pairDevice(BluetoothDevice device) {
        mBTDevice = device;
    }

    /**
     * Check if BTManager module connected to remote device
     *
     * @return true if connected. False, otherwise.
     */
    public boolean isPaired() {
        return mBTDevice != null;
    }

    /**
     * Connecting to desktop server application. This function is called by service after
     * Bluetooth Device retrieved from Discovery Process.
     */
    public synchronized void connect() {

        String command;

        try {

            mSocket = mBTDevice.createRfcommSocketToServiceRecord(AIRPAY_UUID);
            Log.d(TAG, "Connecting to " + AIRPAY_UUID);

            mSocket.connect();
            Log.d(TAG, "Connected to server application");

            InputStream inputStream = mSocket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            OutputStream outputStream = mSocket.getOutputStream();
            PrintStream printer = new PrintStream(outputStream);

            while (true) {
                Log.d(TAG, "Waiting command...");
                while ((command = reader.readLine()) != null) {
                    String response = AirpayApplet.getInstance().process(command);
                    printer.print(response + "\n");
                }
            }

        } catch (IOException e) {
            Log.e(TAG, "Error while connecting to server ", e);
            try {
                mSocket.close();
            } catch (IOException e2) {
                Log.e(TAG, "Error while closing ", e2);
            }
        }
    }

    /**
     * Disconnecting to Desktop Server application
     */
    public synchronized void disconnect() {
        try {
            mSocket.close();
            mBTManager = null;
        } catch (IOException e) {
            Log.e(TAG, "close() of server failed", e);
        }
    }
}

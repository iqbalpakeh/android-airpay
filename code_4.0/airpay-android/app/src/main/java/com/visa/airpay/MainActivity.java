package com.visa.airpay;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.visa.airpay.bluetooth.BTDiscovery;
import com.visa.airpay.bluetooth.BTManager;
import com.visa.airpay.bluetooth.BTService;
import com.visa.airpay.dbase.DBContract;
import com.visa.airpay.transaction.Trx;
import com.visa.airpay.transaction.TrxAdapter;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * For debugging purpose
     */
    private static final String TAG = "MainActivity";

    /**
     * Request to start BTDiscovery to choose Remote Device
     */
    private static final int REQUEST_CONNECT_DEVICE = 1;

    /**
     * Request to enable android bluetooth module
     */
    private static final int REQUEST_ENABLE_BT = 2;

    /**
     * Request to have coarse location from user
     */
    private static final int REQUEST_COARSE_LOCATION = 3;

    /**
     * Reference to device bluetooth adapter
     */
    private BluetoothAdapter mBluetoothAdapter;

    /**
     * View showing total number of transaction
     */
    private TextView mTotalView;

    /**
     * Cursor adapter of the transactions
     */
    private TrxAdapter mTrxAdapter;

    /**
     * Bottom Navigation View implementation
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_start_service:
                    BTService.start(MainActivity.this);
                    return true;
                case R.id.action_stop_service:
                    BTService.stop(MainActivity.this);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_COARSE_LOCATION
            );
        }

        if ((mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()) == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
        }

        mTotalView = findViewById(R.id.total_trx_amount);
        mTrxAdapter = new TrxAdapter(this, null, true);

        ListView trxHistory = findViewById(R.id.history_list_item);
        trxHistory.setEmptyView(findViewById(R.id.empty_view));
        trxHistory.setAdapter(mTrxAdapter);

        getSupportLoaderManager().initLoader(0, null, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.control_discovery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_scan:
                Intent intent = new Intent(this, BTDiscovery.class);
                startActivityForResult(intent, REQUEST_CONNECT_DEVICE);
                return true;
            case R.id.action_clear:
                Trx.clearTable(this);
                mTotalView.setText(new DecimalFormat("#0.00").format(new BigDecimal("0")));
                return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_COARSE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // TODO: what to do if user give access??
                } else {
                    // TODO: what to do if user not giving access??
                }
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras().getString(BTDiscovery.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    BTManager.getInstance().pairDevice(device);
                    Toast.makeText(this, "POS is connected. Press start to pay.", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "POS is connected.");
                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new CursorLoader(this,
                DBContract.Trx.CONTENT_URI, null, null, null, DBContract.Trx._ID + " DESC");
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        if (cursor.getCount() > 0) {
            mTrxAdapter.swapCursor(cursor);
            calculateTotalFromCursor(cursor);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        mTrxAdapter.swapCursor(null);
    }

    /**
     * Calculate total amount of transaction
     *
     * @param cursor return from cursor loader
     */
    private void calculateTotalFromCursor(Cursor cursor) {
        String amount;
        BigDecimal total = new BigDecimal("0");
        cursor.moveToFirst();
        do {
            amount = cursor.getString(cursor.getColumnIndex(DBContract.Trx.COLUMN_AMOUNT));
            total = total.add(new BigDecimal(amount));
        } while (cursor.moveToNext());
        mTotalView.setText(new DecimalFormat("#0.00").format(total));
    }
}

package com.visa.airpay.dbase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    /**
     * Data base name
     */
    private static final String DATABASE_NAME = "airpay_log.db";

    /**
     * Data base version
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * DBHelper single object reference
     */
    private static DBHelper mDBHelper;

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Create single instance object and thread-safe
     *
     * @param context of application
     * @return single DBHelper object
     */
    public static synchronized DBHelper getInstance(Context context) {
        if (mDBHelper == null) mDBHelper = new DBHelper(context);
        return mDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String SQL_CREATE_TRACE_TABLE = "CREATE TABLE " + DBContract.Trx.TABLE_NAME + " ("
                + DBContract.Trx.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBContract.Trx.COLUMN_TIMESTAMP + " TEXT NOT NULL, "
                + DBContract.Trx.COLUMN_AMOUNT + " TEXT NOT NULL, "
                + DBContract.Trx.COLUMN_MERCHANT + " TEXT NOT NULL);";

        db.execSQL(SQL_CREATE_TRACE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // nothing until next db upgrade
    }

}

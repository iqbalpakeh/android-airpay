package com.visa.airpay.gui;

import javafx.beans.property.SimpleStringProperty;

public class Item {

    public static String KEY_NAME = "name"; // it must be getName()

    public static String KEY_PRICE = "price"; // it must be getPrice()

    private SimpleStringProperty mName;

    private SimpleStringProperty mPrice;

    private double mTotalPrice;

    public static Item newInstance() {
        return new Item();
    }

    private Item() {
        mName = new SimpleStringProperty();
        mPrice = new SimpleStringProperty();
    }

    public void setName(String name) {
        this.mName.set(name);
    }

    public String getName() {
        return mName.get();
    }

    public void setPrice(String price) {
        this.mPrice.set(price);
    }

    public String getPrice() {
        return mPrice.get();
    }

    public double getTotalPrice() {
        return mTotalPrice;
    }

    public void setTotalPrice(double price) {
        mTotalPrice = price;
    }
}

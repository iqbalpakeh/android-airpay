package com.visa.airpay.gui;

import com.visa.airpay.payment.AirpayRunnable;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;

import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;

public class Controller implements Initializable, AirpayRunnable.AirpayCallback {

    @FXML
    private TableColumn mItemNameCol;

    @FXML
    private TableColumn mItemPriceCol;

    @FXML
    private TableView mItemTable;

    @FXML
    private Label mLabelProduct;

    @FXML
    private Label mLabelPrice;

    @FXML
    private Label mLabelTotalPrice;

    @FXML
    private Label mLabelInstruction;

    @FXML
    private Button mButtonPay;

    @FXML
    private Button mButtonScan;

    @FXML
    private HBox mPaneScan;

    @FXML
    private VBox mPanePayOption;

    @FXML
    private VBox mPanePair;

    @FXML
    private VBox mPaneProcess;

    @FXML
    private TextArea mTextAreaLog;

    private double mTotalPrice;

    private ObservableList<Item> mItems;

    private Map<String, Double> mProductDB = new HashMap<>();

    private Iterator mProducts;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        mTotalPrice = 0;
        mItems = FXCollections.observableArrayList();

        mItemNameCol.setCellValueFactory(new PropertyValueFactory<Item, String>(Item.KEY_NAME));
        mItemPriceCol.setCellValueFactory(new PropertyValueFactory<Item, Double>(Item.KEY_PRICE));

        mPaneScan.setVisible(true);
        mPanePayOption.setVisible(false);
        mPanePayOption.setDisable(true);
        mPanePair.setVisible(false);
        mPanePair.setDisable(true);
        mPaneProcess.setVisible(false);
        mPaneProcess.setDisable(true);
        mButtonPay.setDisable(true);
        mProductDB.put("UHT 100% ORGANIC MILK", 4.90);
        mProductDB.put("NAT P/BUTR-CRUNCHY", 4.75);
        mProductDB.put("CAPILANO HONEY", 8.40);
        mProductDB.put("OATMEAL RF-QK/COOK", 2.95);
        mProductDB.put("EXT VIRGIN OLIVE OIL", 8.90);
        mProductDB.put("NAT B/RICE-S/GRAIN", 10.80);
        mProductDB.put("F/EG DHA  03+B/CA 10S", 3.25);

        mItemTable.setPlaceholder(new Label(""));

        mProducts = mProductDB.entrySet().iterator();
    }

    public void btnPay_Click() {
        mPaneScan.setVisible(false);
        mLabelInstruction.setVisible(false);
        mButtonScan.setVisible(false);
        mButtonScan.setDisable(true);
        mPanePayOption.setVisible(true);
        mPanePayOption.setDisable(false);
        mButtonPay.setDisable(true);
    }

    public void scanItems() {

        if (mProducts.hasNext()) {

            Map.Entry nextProduct = (Map.Entry) mProducts.next();
            Item item = Item.newInstance();

            String proName = nextProduct.getKey().toString();
            item.setName(proName);

            double value = (Double) nextProduct.getValue();
            item.setPrice(String.format("%.2f", value));

            mTotalPrice += value;
            item.setTotalPrice(mTotalPrice);

            mItems.add(item);
            mItemTable.setItems(mItems);
            mLabelProduct.setText(proName);
            mLabelPrice.setText("$" + String.format("%.2f", value));

            mLabelTotalPrice.setText("$" + String.format("%.2f", mTotalPrice));
            mButtonPay.setText("Pay $" + String.format("%.2f", mTotalPrice));

            mButtonPay.setDisable(false);
        }
    }

    public void init_payment() {

        mProducts = mProductDB.entrySet().iterator();
        mTotalPrice = 0;
        mItems.clear();
        mItemTable.refresh();
        mLabelProduct.setText("Please scan an item...");
        mLabelPrice.setText("");
        mLabelTotalPrice.setText("");
        mButtonPay.setText("");

        mPaneScan.setVisible(true);
        mPanePayOption.setVisible(false);
        mPanePayOption.setDisable(true);
        mPanePair.setVisible(false);
        mPanePair.setDisable(true);
        mPaneProcess.setVisible(false);
        mPaneProcess.setDisable(true);
        mButtonPay.setDisable(true);
        mLabelInstruction.setVisible(true);
        mLabelInstruction.setDisable(false);
        mButtonScan.setVisible(true);
        mButtonScan.setDisable(false);
    }

    public void btnBluetooth_Click() {

        // Show waiting UI
        mPanePayOption.setVisible(false);
        mPanePayOption.setDisable(true);
        mPanePair.setVisible(true);
        mPanePair.setDisable(false);

        // Start Airpay Kernel
        AirpayRunnable airpayRunnable = new AirpayRunnable(String.valueOf(mTotalPrice));
        airpayRunnable.setOnCVMVerified(this);
        Thread thread = new Thread(airpayRunnable);
        thread.start();
    }

    @Override
    public void onCVMVerified() {
        if (mPanePair.isVisible()) {
            mPanePair.setVisible(false);
            mPaneProcess.setVisible(true);
            mPaneProcess.setDisable(false);
        }
    }

    @Override
    public void printLog(String msg) {
        mTextAreaLog.appendText(msg + "\r\n");
    }

    public void btnClearLogs(){
        mTextAreaLog.clear();
    }

}

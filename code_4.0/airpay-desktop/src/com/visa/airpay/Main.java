package com.visa.airpay;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    // IMPORTANT:
    //
    // 1. Sample code between java desktop and android
    //    http://luugiathuy.com/2011/02/android-java-bluetooth/
    //    http://www.aviyehuda.com/blog/2010/01/08/connecting-to-bluetooth-devices-with-java/
    //    https://stackoverflow.com/questions/28740810/bluecove-check-if-device-is-paired
    //
    // 2. Working BlueCode version on mac
    //    https://stackoverflow.com/questions/22892738/mac-os-and-java-bluetooth
    //    https://code.google.com/archive/p/bluecove/issues/134
    //
    // 3. Someone fork the repository from bluecove.org and maintain
    //    on github (not maintain by the group anymore??)
    //    https://github.com/machaval/bluecove
    //
    // 4. Official Java Api from Oracle
    //    http://www.oracle.com/technetwork/articles/javame/index-156193.html
    //    http://www.oracle.com/technetwork/java/javame/tech/index-140411.html
    //
    // 5. Bluecove quick tutorial
    //    http://www.bluecove.org/bluecove/apidocs/overview-summary.html#DeviceDiscovery
    //
    public static void main(String[] args) {

        // NOTE #1:
        // To make this laptop discoverable during discovery state on mobile phone seems impossible.
        // Try to learn the API in more details.
        //
        // http://www.oracle.com/technetwork/java/javame/tech/index-140411.html
        //
        // For the moment, to help pc discovered by mobile phone, we need to use laptop bluetooth
        // system manager instead of triggered by this application.

        // NOTE #2:
        // After several time testing with android, it seems like this application is not recognized anymore
        // by android apps. The reason is still unknown (UPDATED, IT'S SOLVED! Read next sentence on this note).
        //
        // To solve this issue: Mac need to be restarted. This could mean that some register are not properly
        // cleared after finish. But, not sure which API solving this issue.
        //
        // ....
        //
        // After research the solution, it's known that bluecove has implementation bug on close() function. Thus,
        // use BlueCoveImpl.shutdown() instead. This is described at this stackoverflow discussion
        //
        // https://stackoverflow.com/questions/16372206/bluecove-restart-bluetooth-stack-programmatically
        //

        // NOTE #3:
        // // -- Sample code for waiting user input
        // // -- need to add code in gradle build
        // // -- run {
        // //        standardInput = System.in
        // //    }
        // //
        // Scanner input = new Scanner(System.in);
        // int score = 0;
        // while (score >= 0) {
        //     score = input.nextInt();
        //     System.out.println("Score = " + score);
        // }
        // input.close();

        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/ui.fxml"));
        primaryStage.setTitle("AirPay - POS Demo");
        primaryStage.setScene(new Scene(root, 1200, 600));
        primaryStage.setMaximized(true);
        primaryStage.setMinHeight(900);
        primaryStage.setMinWidth(1600);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("AirPay is closed by user");
    }
}

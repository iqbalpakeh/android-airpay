package com.visa.airpay.payment;

import com.intel.bluetooth.BlueCoveImpl;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import java.io.IOException;

public class AirpayRunnable implements Runnable {

    /**
     * Amount Authorized for current transaction
     */
    private String mAmountAuthorized;

    /**
     * Reference to object implementing this callback
     */
    private AirpayCallback mCallback;

    /**
     * Constructor to start airpay thread to do emv nextgen transaction
     *
     * @param amountAuthorized of current transaction
     */
    public AirpayRunnable(String amountAuthorized) {
        this.mAmountAuthorized = amountAuthorized;
    }

    @Override
    public void run() {

        StreamConnectionNotifier notifier;
        StreamConnection connection;

        try {

            // Prepare Bluetooth module
            LocalDevice.getLocalDevice().setDiscoverable(DiscoveryAgent.GIAC);
            UUID uuid = new UUID(AirpayKernel.UUID, false);
            System.out.println(uuid.toString());
            mCallback.printLog(uuid.toString());
            String url = "btspp://localhost:" + uuid.toString() + ";name=RemoteBluetooth";
            notifier = (StreamConnectionNotifier) Connector.open(url);
            System.out.println("waiting for connection...");
            mCallback.printLog("waiting for connection...");
            connection = notifier.acceptAndOpen();

            // Start Command exchange
            AirpayKernel.getInstance(connection, mCallback).execute(mAmountAuthorized);

            // Close system
            AirpayKernel.getInstance().kill();
            notifier.close();
            connection.close();
            BlueCoveImpl.shutdown();
            mCallback.onCVMVerified();

        } catch (BluetoothStateException e) {
            System.out.println("Bluetooth is not turned on.");
            mCallback.printLog("Bluetooth is not turned on.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the implementer of airpay callback method
     *
     * @param callback callback implementer
     */
    public void setOnCVMVerified(AirpayCallback callback) {
        mCallback = callback;
    }

    /**
     * Interface to communicate between airpay kernel and gui
     */
    public interface AirpayCallback {

        /**
         * Call this function once CVM is verified by User
         * to complete transaction
         */
        void onCVMVerified();

        /**
         * Call this function to print log on GUI
         */
        void printLog(String msg);
    }
}

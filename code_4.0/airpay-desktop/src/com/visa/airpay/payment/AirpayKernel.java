package com.visa.airpay.payment;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.microedition.io.StreamConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;

public class AirpayKernel {

    /**
     * Airpay Payment system UUID
     */
    public static final String UUID = "04c6093b00001000800000805f9b34fd";

    /**
     * Bluetooth connection object
     */
    private StreamConnection mConnection;

    /**
     * Log interface object
     */
    private AirpayRunnable.AirpayCallback mCallBack;

    /**
     * Static reference to AirpayKernel
     */
    private static AirpayKernel mKernel;

    /**
     * Get singleton instance of AirpayKernel
     *
     * @param connection of bluetooth
     * @return AirpayKernel object
     */
    public static synchronized AirpayKernel getInstance(StreamConnection connection, AirpayRunnable.AirpayCallback mCallBack) {
        if (mKernel == null) mKernel = new AirpayKernel(connection, mCallBack);
        return mKernel;
    }

    private AirpayKernel(StreamConnection connection, AirpayRunnable.AirpayCallback callBack) {
        mConnection = connection;
        mCallBack = callBack;
    }

    /**
     * Get singleton instance of AirpayKernel
     *
     * @return AirpayKernel object
     */
    public static synchronized AirpayKernel getInstance() {
        if (mKernel == null) throw new InvalidParameterException("Instance is not yet created");
        return mKernel;
    }


    /**
     * Clear kernel instance
     */
    public void kill() {
        mKernel = null;
    }

    /**
     * Send all necessary command for one EMV transaction
     */
    public void execute(String amountAuthorized) {

        AirpayCommands commands = new AirpayCommands();
        String command;
        String response;

        try {

            InputStream inputStream = mConnection.openInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            OutputStream out = mConnection.openOutputStream();
            PrintStream printer = new PrintStream(out);

            String value = amountAuthorized;
            String str_date;
            String str_time;
            String str_xml;

            mCallBack.printLog("\nTransaction Amount = " + value + " USD\n");

            // Get current tdate time
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            String datetime = dtf.format(now);

            // split into date and time
            String[] parts = datetime.split(" ");
            str_date = parts[0];
            str_time = parts[1];

            str_xml = commands.BeginTransaction(value, str_date, str_time);
            if (str_xml == "") {
                mCallBack.printLog("Error in POI Begin Transsaction !");
                throw new InvalidParameterException("Error in POI Begin Transsaction !");
            }
            mCallBack.printLog(format(str_xml));

            str_xml = commands.ask_for_card();
            // call gui ?
            mCallBack.printLog(format(str_xml));

            command = commands.kernel_open("NGSE") + "\n";
            printer.print(command);
            while ((response = reader.readLine()) != null) {
                if (!command.equals("")) mCallBack.printLog("Command = " + format(command));
                if (!response.equals("")) mCallBack.printLog("Response = " + format(response) + "\n");
                break;
            }

            command = commands.kernel_data_present_card() + "\n";
            printer.print(command);
            while ((response = reader.readLine()) != null) {
                if (!command.equals("")) mCallBack.printLog("Command = " + format(command));
                if (!response.equals("")) mCallBack.printLog("Response = " + format(response) + "\n");
                break;
            }

            command = commands.kernel_close("NGSE") + "\n";
            printer.print(command);
            while ((response = reader.readLine()) != null) {
                if (!command.equals("")) mCallBack.printLog("Command = " + format(command));
                if (!response.equals("")) mCallBack.printLog("Response = " + format(response) + "\n");
                break;
            }

            // Kernel open 2nd time for CDCVM
            command = commands.kernel_open("A000000000B16F04") + "\n";
            printer.print(command);
            while ((response = reader.readLine()) != null) {
                if (!command.equals("")) mCallBack.printLog("Command = " + format(command));
                if (!response.equals("")) mCallBack.printLog("Response = " + format(response) + "\n");
                break;
            }

            command = commands.kernel_data_processing(value, str_date, str_time) + "\n";
            printer.print(command);
            while ((response = reader.readLine()) != null) {
                if (!command.equals("")) mCallBack.printLog("Command = " + format(command));
                if (!response.equals("")) mCallBack.printLog("Response = " + format(response) + "\n");
                break;
            }

            // after this, the card/phone should prompt user for ack and wait for command again?
            command = commands.kernel_signalling("positive_acknowledgement") + "\n";
            printer.print(command);
            while ((response = reader.readLine()) != null) {
                if (!command.equals("")) mCallBack.printLog("Command = " + format(command));
                if (!response.equals("")) mCallBack.printLog("Response = " + format(response) + "\n");
                break;
            }

            str_xml = commands.remove_card();
            mCallBack.printLog(format(str_xml));
            // need to inform POS ? ?

            mCallBack.printLog("==== Waiting for CDCVM ====\n");

            str_xml = commands.ask_for_card();
            mCallBack.printLog(format(str_xml));

            // Kernel open 3rd time for completion
            command = commands.kernel_open("A000000000B16F04") + "\n";
            printer.print(command);
            while ((response = reader.readLine()) != null) {
                if (!command.equals("")) mCallBack.printLog("Command = " + format(command));
                if (!response.equals("")) mCallBack.printLog("Response = " + format(response) + "\n");
                break;
            }

            command = commands.kernel_signalling("") + "\n";
            printer.print(command);
            while ((response = reader.readLine()) != null) {
                if (!command.equals("")) mCallBack.printLog("Command = " + format(command));
                if (!response.equals("")) mCallBack.printLog("Response = " + format(response) + "\n");
                break;
            }

            command = commands.kernel_signalling("positive_acknowledgement") + "\n";
            printer.print(command);
            while ((response = reader.readLine()) != null) {
                if (!command.equals("")) mCallBack.printLog("Command = " + format(command));
                if (!response.equals("")) mCallBack.printLog("Response = " + format(response) + "\n");
                break;
            }

            command = commands.kernel_data_completion() + "\n";
            printer.print(command);
            while ((response = reader.readLine()) != null) {
                if (!command.equals("")) mCallBack.printLog("Command = " + format(command));
                if (!response.equals("")) mCallBack.printLog("Response = " + format(response) + "\n");
                break;
            }

            String timestamp = String.valueOf(System.currentTimeMillis());

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.valueOf(timestamp));
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US);
            String time = fmt.format(calendar.getTime());

            mCallBack.printLog("Transaction completed on " + time + "\n");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Printing XML in good format
     *
     * @param xml data exchange
     * @return good format xml
     */
    private String format(String xml) {

        if (!xml.startsWith("<")) {
            return xml;
        }

        try {

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            Document doc = db.parse(is);

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            return writer.getBuffer().toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

}

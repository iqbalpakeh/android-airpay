package com.visa.airpay.dbase;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class DBContract {

    /**
     * Content Authority
     */
    public static final String CONTENT_AUTHORITY = "com.visa.airpay.dbase";

    /**
     * Base URI to access content provider
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /**
     * Empty private constructor to stop any unwanted object creation
     */
    private DBContract() {
    }

    /**
     * Constant used by Trx table
     */
    public static final class Trx implements BaseColumns {

        /**
         * Name of database transaction table
         */
        public final static String TABLE_NAME = "TRANSACTION_TABLE";

        /**
         * Unique ID number (only for use in the database table).
         * Type: INTEGER
         */
        public final static String COLUMN_ID = BaseColumns._ID;

        /**
         * Trx timestamp
         * Type: TEXT
         */
        public final static String COLUMN_TIMESTAMP = "TIMESTAMP";

        /**
         * Trx amount
         * Type: TEXT
         */
        public final static String COLUMN_AMOUNT = "AMOUNT";

        /**
         * Trx merchant
         * Type: TEXT
         */
        public final static String COLUMN_MERCHANT = "MERCHANT";

        /**
         * Create CONTENT_URI
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

        /**
         * Builds URIs on insertion
         *
         * @param id of selected row
         * @return concatenated uri
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        /**
         * Projection used by Query
         *
         * @return columns projection
         */
        public static String[] projection() {
            return new String[]{
                    COLUMN_ID,
                    COLUMN_TIMESTAMP,
                    COLUMN_AMOUNT,
                    COLUMN_MERCHANT
            };
        }

        /**
         * Sort mode used by Query
         *
         * @return SQL sorted mode
         */
        public static String sort() {
            return COLUMN_TIMESTAMP + " ASC ";
        }
    }

}

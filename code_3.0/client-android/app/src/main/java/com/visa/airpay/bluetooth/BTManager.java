package com.visa.airpay.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.visa.airpay.payment.AirpayApplet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.security.InvalidParameterException;
import java.util.UUID;

public class BTManager {

    /**
     * Used for debugging
     */
    private static final String TAG = "BTManager";

    /**
     * Airpay server UUID
     */
    private static final UUID AIRPAY_UUID = UUID.fromString("04c6093b-0000-1000-8000-00805f9b34fd");

    /**
     * Bluetooth adapter
     */
    private BluetoothAdapter mAdapter;

    /**
     * Reference to remote bluetooth device that have airpay server (Desktop Application)
     */
    private BluetoothDevice mBTDevice;

    /**
     * Singletone reference object
     */
    private static BTManager mBTManager;

    /**
     * Bluetooth socket reference
     */
    private BluetoothSocket mSocket;


    /**
     * Singletone function to retrieve BTManager object
     *
     * @return BTManager object
     */
    public static synchronized BTManager getInstance() {
        if(mBTManager == null) mBTManager = new BTManager();
        return mBTManager;
    }

    private BTManager() {
        if (mAdapter == null || mBTDevice == null)
            throw new InvalidParameterException("Please call getInstance(BluetoothDevice device)");
    }

    /**
     * Singletone function to create BTManager object and provide the remote bluetooth device object
     * found during bluetooth discovery
     *
     * @param device remote bluetooth device
     * @return BTManager object
     */
    public  static synchronized BTManager getInstance(BluetoothDevice device) {
        if(mBTManager == null) mBTManager = new BTManager(device);
        return mBTManager;
    }

    private BTManager(BluetoothDevice device) {
        mBTDevice = device;
        mAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    /**
     * Connecting to desktop server application. This function is called by service after
     * Bluetooth Device retrieved from Discovery Process.
     */
    public synchronized void connect() {

        String command;

        try {

            mSocket = mBTDevice.createRfcommSocketToServiceRecord(AIRPAY_UUID);
            Log.d(TAG, "Connecting to " + AIRPAY_UUID);

            mSocket.connect();
            Log.d(TAG, "Connected to server application");

            InputStream inputStream = mSocket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            OutputStream outputStream = mSocket.getOutputStream();
            PrintStream printer = new PrintStream(outputStream);

            while(true) {
                Log.d(TAG, "Waiting command...");
                while ((command = reader.readLine()) != null) {
                    String response = AirpayApplet.getInstance().process(command);
                    printer.print(response + "\n");
                }
            }

        } catch (IOException e) {
            Log.e(TAG, "Error while connecting to server ", e);
            try {
                mSocket.close();
            } catch (IOException e2) {
                Log.e(TAG, "Error while closing ", e2);
            }
        }
    }

    /**
     * Disconnecting to Desktop Server application
     */
    public synchronized void disconnect() {
        try {
            mSocket.close();
            mBTManager = null;
        } catch (IOException e) {
            Log.e(TAG, "close() of server failed", e);
        }
    }
}

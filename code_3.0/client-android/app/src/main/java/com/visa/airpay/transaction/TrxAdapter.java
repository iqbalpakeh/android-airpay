package com.visa.airpay.transaction;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visa.airpay.R;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TrxAdapter extends CursorAdapter {

    public TrxAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).
                inflate(R.layout.transaction_detail, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        Trx trx = Trx.build(cursor);
        String note = prepareTrxNote(trx);
        String date = prepareDate(trx);
        String time = prepareTime(trx);
        String amount = prepareAmount(trx);
        String avatar = prepareTrxAvatar(trx);
        Drawable drawable = prepareTrxDrawable(trx);

        TextView typeTextView = view.findViewById(R.id.transaction_note);
        typeTextView.setText(note);

        TextView amountTextView = view.findViewById(R.id.transaction_amount);
        amountTextView.setText(amount);

        TextView dateTextView = view.findViewById(R.id.transaction_date);
        dateTextView.setText(date);

        TextView timeTextView = view.findViewById(R.id.transaction_time);
        timeTextView.setText(time);

        TextView avatarTextView = view.findViewById(R.id.avatar);
        avatarTextView.setText(avatar);
        avatarTextView.setBackground(drawable);

    }

    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    /**
     * Prepare avatar message to shows on the transaction details
     *
     * @param trx object
     * @return avatar message
     */
    private String prepareTrxAvatar(Trx trx) {
        // only bluetooth supported
        return "BLT";
    }

    /**
     * Select drawable object based on transaction to shows on the transaction details
     *
     * @param trx object
     * @return drawable object
     */
    private Drawable prepareTrxDrawable(Trx trx) {
        // only bluetooth supported
        return mContext.getResources().getDrawable(R.drawable.avatar_nfc_payment);
    }

    /**
     * Prepare transaction note to shows on the transaction details
     *
     * @param trx object
     * @return transaction note
     */
    private String prepareTrxNote(Trx trx) {
        return trx.getMerchant() + " AirPay";
    }

    /**
     * Convert Unix Time format to readable Date format
     *
     * @param trx object
     * @return readable data format
     */
    private String prepareDate(Trx trx) {
        String date;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.valueOf(trx.getTimestamp()));
        SimpleDateFormat fmt = new SimpleDateFormat("MMM d, yyyy", Locale.US);
        date = fmt.format(calendar.getTime());
        return date;
    }

    /**
     * Convert Unix Time format to readable Time format
     *
     * @param trx object
     * @return readable time format
     */
    private String prepareTime(Trx trx) {
        String time;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.valueOf(trx.getTimestamp()));
        SimpleDateFormat fmt = new SimpleDateFormat("h:mm a", Locale.US);
        time = fmt.format(calendar.getTime());
        return time;
    }

    /**
     * Prepare amount in corrected format
     *
     * @param trx object
     * @return corrected format of amount
     */
    private String prepareAmount(Trx trx) {
        return "$ "+ new DecimalFormat("#0.00").format(new BigDecimal(trx.getAmount()));
    }

}

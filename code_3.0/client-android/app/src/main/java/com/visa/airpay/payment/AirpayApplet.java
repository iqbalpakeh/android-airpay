package com.visa.airpay.payment;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.security.InvalidParameterException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class AirpayApplet {

    private final String TAG = "AirpayApplet";

    /**
     * Interface to communicate between applet and android service
     */
    public interface AirpayCallback {

        /**
         * Call this function once CVM is verified by User
         * to start triggering notification
         */
        void onCVMVerified(String amount);

    }

    /**
     * Reference to object implementing this callback
     */
    private AirpayCallback mCallback;

    /**
     * Singletone reference to AirpayApplet object
     */
    private static AirpayApplet mApplet;

    /**
     * Object contain Airpay Response
     */
    private AirpayResponse mResponses;

    /**
     * Amount of the transaction
     */
    private String mAmount;

    /**
     * To retrieve singleton object of airpay object
     *
     * @return airpay object
     */
    public synchronized static AirpayApplet getInstance() {
        if (mApplet == null) mApplet = new AirpayApplet();
        return mApplet;
    }

    private AirpayApplet() {
        mResponses = new AirpayResponse();
    }

    /**
     * Delete Airpay Applet instance
     */
    public void deleteInstance() {
       mApplet = null;
    }

    /**
     * Set the implementer of airpay callback method
     *
     * @param callback callback implementer
     */
    public void setOnCVMVerified(AirpayCallback callback) {
        Log.d(TAG, "callback = " + callback.getClass().getName());
        mCallback = callback;
    }

    /**
     * Airpay applet method to process all the command coming from server desktop
     *
     * @param command from desktop server
     * @return of airpay applet object
     */
    public String process(String command) {

        Log.d(TAG, "Command = " + command);

        if (command.equals(mResponses.kernel_open_present_card)) {
            Log.d(TAG, "Response = " + mResponses.open_response("01"));
            return mResponses.open_response("01");
        }

        if (command.equals(mResponses.kernel_data_present_card)) {
            Log.d(TAG, "Response = " + mResponses.card_data_1());
            return mResponses.card_data_1();
        }

        if (command.equals(mResponses.kernel_open_processing)) {
            Log.d(TAG, "Response = " + mResponses.open_response(""));
            return mResponses.open_response("");
        }

        if (command.startsWith(mResponses.kernel_data_processing)) {
            Log.d(TAG, "Response = " + mResponses.card_data_2());
            mAmount = extractAmount(command);
            return mResponses.card_data_2();
        }

        if (command.equals(mResponses.kernel_signalling_empty)) {
            Log.d(TAG, "Response = " + mResponses.card_data_3());
            return mResponses.card_data_3();
        }

        if (command.equals(mResponses.kernel_data_completion)) {
            Log.d(TAG, "Response = " + mResponses.card_data_4());
            triggerCvmAndWait(mAmount);
            return mResponses.card_data_4();
        }

        Log.d(TAG, "Response = empty");
        return "";
    }

    /**
     * Trigger the CVM notification and wait until user finish
     *
     * @param amount of transaction
     */
    private void triggerCvmAndWait(String amount) {

        // Trigger CVM Notification
        mCallback.onCVMVerified(amount);

        // Wait user to complete CVM
        String flag = CvmFlag.getInstance().takeFlag();
        if (!flag.equals(CvmFlag.FLAG)) throw new InvalidParameterException("Different flag value");
    }

    /**
     * Extract amount from xml document
     *
     * @param xml document from server
     * @return amount
     */
    private String extractAmount(String xml) {
        String amount = "";
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("emv.poi:Amount");
            Node node = nodes.item(0);
            amount = node.getTextContent();
        } catch (Exception e){
            e.printStackTrace();
        }
        return amount;
    }
}

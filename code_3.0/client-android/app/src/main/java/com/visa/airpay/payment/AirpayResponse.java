package com.visa.airpay.payment;

import java.security.InvalidParameterException;

public class AirpayResponse {

    protected final String kernel_open_present_card = "<emv.event.CAL:Kernel_Open>" +
            "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
            "<emv.scard:ActiveInterfaceID>Contactless</emv.scard:ActiveInterfaceID>" +
            "<emv.scard:CCEID>NGSE</emv.scard:CCEID></emv.event.CAL:Kernel_Open>";

    protected final String kernel_data_present_card = "<emv.event.CAL:Kernel_Data>" +
            "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
            "<emv.tx.dcm:MessageChoiceCDO>" +
            "<Extended><HeaderList>" +
            "<Header>ALDIL</Header>" +
            "</HeaderList><CDIL><DataID>emv.ngse:SvIDList</DataID>" +
            "<DataID>emv.ngse:ProposedAmount</DataID>" +
            "<DataID>emv.ngse:InterfaceIDList</DataID>" +
            "<DataID>emv.ngse:FormFactor</DataID>" +
            "</CDIL><UnprotectedALPayloadContainer/>" +
            "</Extended><Format>Extended</Format>" +
            "</emv.tx.dcm:MessageChoiceCDO></emv.event.CAL:Kernel_Data>";

    protected final String kernel_open_processing = "<emv.event.CAL:Kernel_Open>" +
            "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
            "<emv.scard:ActiveInterfaceID>Contactless</emv.scard:ActiveInterfaceID>" +
            "<emv.scard:CCEID>A000000000B16F04</emv.scard:CCEID></emv.event.CAL:Kernel_Open>";

    protected final String kernel_data_processing = "<emv.event.CAL:Kernel_Data>" +
            "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
            "<emv.tx.dcm:MessageChoiceCDO>" +
            "<Extended>" +
            "<HeaderList>" +
            "<Header>ALDIL</Header>" +
            "<Header>ALPayload</Header>" +
            "</HeaderList>" +
            "<CDIL>" +
            "<DataID>emv.card:PAN</DataID>" +
            "<DataID>emv.card:ExpiryDate</DataID>" +
            "<DataID>emv.card:AuthCDO</DataID>" +
            "<DataID>emv.card:ConditionalAuthRequestDataIDList</DataID>" +
            "<DataID>emv.card:ConditionalTransactionCompletionDataIDList</DataID>" +
            "<DataID>emv.card:NextCVMRequirementCDO</DataID>" +
            "</CDIL>";

    protected final String kernel_signalling_empty = "<emv.event.CAL:Kernel_Signaling>" +
            "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
            "<emv.scard:ProtocolLayerCDO>" +
            "<SignallingControlList/>" +
            "<ResumeInterfaceID/>" +
            "<DisconnectIndicator>Empty</DisconnectIndicator>" +
            "</emv.scard:ProtocolLayerCDO>" +
            "</emv.event.CAL:Kernel_Signaling>";

    protected final String kernel_data_completion = "<emv.event.CAL:Kernel_Data>" +
            "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
            "<emv.tx.dcm:MessageChoiceCDO>" +
            "<Extended>" +
            "<HeaderList>" +
            "<Header>ALDIL</Header>" +
            "</HeaderList>" +
            "<CDIL>" +
            "<DataID>emv.card:AuthCDO</DataID>" +
            "<DataID>emv.card:NextCVMRequirementCDO</DataID>" +
            "</CDIL>" +
            "<UnprotectedALPayloadContainer/>" +
            "</Extended>" +
            "<Format>Extended</Format>" +
            "</emv.tx.dcm:MessageChoiceCDO>" +
            "</emv.event.CAL:Kernel_Data>";

    protected String open_response(String interop) {
        if (interop.equals("01")) {
            return "<emv.event.SimulatedCard:Open_Response>" +
                    "<emv.event:DestinationModule>CAL</emv.event:DestinationModule>" +
                    "<emv.card:OpenDataContainer>" +
                    "<emv.card:DataEncoding>Extended</emv.card:DataEncoding>" +
                    "<emv.card:InteropVersionList>" +
                    "<InteropVersion>01</InteropVersion>" +
                    "</emv.card:InteropVersionList>" +
                    "</emv.card:OpenDataContainer>" +
                    "</emv.event.SimulatedCard:Open_Response>";
        } else if (interop.equals("")) {
            return "<emv.event.SimulatedCard:Open_Response>" +
                    "<emv.event:DestinationModule>CAL</emv.event:DestinationModule>" +
                    "<emv.card:OpenDataContainer>" +
                    "<emv.card:DataEncoding>Extended</emv.card:DataEncoding>" +
                    "<emv.card:InteropVersionList qual=\"AvailableButEmpty\"/>" +
                    "</emv.card:OpenDataContainer>" +
                    "</emv.event.SimulatedCard:Open_Response>";
        } else if (interop == null) {
            return "<emv.event.SimulatedCard:Open_Response>" +
                    "<emv.event:DestinationModule>CAL</emv.event:DestinationModule>" +
                    "<emv.card:OpenDataContainer>" +
                    "<emv.card:DataEncoding>Extended</emv.card:DataEncoding>" +
                    "</emv.card:OpenDataContainer>" +
                    "</emv.event.SimulatedCard:Open_Response>";
        }
        throw new InvalidParameterException();
    }

    protected String card_data_1() {
        return "<emv.event.SimulatedCard:Card_Data>" +
                "<emv.event:DestinationModule>CAL</emv.event:DestinationModule>" +
                "<emv.card:MessageChoiceCDO>" +
                "<Extended>" +
                "<HeaderList>" +
                "<Header>ALPayload</Header>" +
                "</HeaderList>" +
                "<UnprotectedALPayloadContainer>" +
                "<emv.ngse:SvIDList>" +
                "<SvIDCDO>" +
                "<SvID>A000000000B16Flow04</SvID>" +
                "<CEID>A000000000B16F04</CEID>" +
                "<SvIDGroup/>" +
                "<Label>B16Flow04</Label>" +
                "<CurrencyCode>840</CurrencyCode>" +
                "<ConfirmationRequired>False</ConfirmationRequired>a" +
                "<Priority>1</Priority>" +
                "<Technology>NextGen</Technology>" +
                "<CVMethodIDList/>" +
                "<TRMCapabilityList/>" +
                "<PreferredLanguage>en</PreferredLanguage>" +
                "<PreferredName>B16Flow04</PreferredName>" +
                "<EligibleAcquirerIDList>" +
                "<EligibleAcquirerID>Acquirer1</EligibleAcquirerID>" +
                "</EligibleAcquirerIDList>" +
                "<Rescue>Empty</Rescue>" +
                "<InteropVersionList>" +
                "<InteropVersion>01</InteropVersion>" +
                "</InteropVersionList>" +
                "<AdditionalKDIL>" +
                "<DataID>emv.tx.cvm:KernelSupportedCVMList</DataID>" +
                "</AdditionalKDIL>" +
                "<DataEncoding>Extended</DataEncoding>" +
                "<CountryCode>840</CountryCode>" +
                "<IIN>112233</IIN>" +
                "</SvIDCDO>" +
                "</emv.ngse:SvIDList>" +
                "<emv.ngse:ProposedAmount qual=\"Unavailable\"/>" +
                "<emv.ngse:InterfaceIDList>" +
                "<InterfaceID>Contactless</InterfaceID>" +
                "</emv.ngse:InterfaceIDList>" +
                "<emv.ngse:FormFactor>Mobile</emv.ngse:FormFactor>" +
                "</UnprotectedALPayloadContainer>" +
                "</Extended>" +
                "<Format>Extended</Format>" +
                "</emv.card:MessageChoiceCDO>" +
                "</emv.event.SimulatedCard:Card_Data>";
    }

    protected String card_data_2() {
        return "<emv.event.SimulatedCard:Card_Data>" +
                "<emv.event:DestinationModule>CAL</emv.event:DestinationModule>" +
                "<emv.card:MessageChoiceCDO>" +
                "<Extended>" +
                "<HeaderList>" +
                "<Header>ALPayload</Header>" +
                "</HeaderList>" +
                "<UnprotectedALPayloadContainer>" +
                "<emv.card:PAN>1234567890123456</emv.card:PAN>" +
                "<emv.card:ExpiryDate>2050-01</emv.card:ExpiryDate>" +
                "<emv.card:AuthCDO qual=\"NotYetAvailable\"/>" +
                "<emv.card:ConditionalAuthRequestDataIDList qual=\"Unavailable\"/>" +
                "<emv.card:ConditionalTransactionCompletionDataIDList qual=\"Unavailable\"/>" +
                "<emv.card:NextCVMRequirementCDO qual=\"NotYetAvailable\"/>" +
                "</UnprotectedALPayloadContainer>" +
                "</Extended>" +
                "<Format>Extended</Format>" +
                "</emv.card:MessageChoiceCDO>" +
                "<emv.card:ProtocolLayerCDO>" +
                "<SignallingControlList>" +
                "<SignallingControl>SuspendRequest</SignallingControl>" +
                "</SignallingControlList>" +
                "<ResumeInterfaceID>Contactless</ResumeInterfaceID>" +
                "<DisconnectIndicator>True</DisconnectIndicator>" +
                "<ResumeInterfaceCEID>A000000000B16F04</ResumeInterfaceCEID>" +
                "</emv.card:ProtocolLayerCDO>" +
                "</emv.event.SimulatedCard:Card_Data>";
    }

    protected String card_data_3() {
        return "<emv.event.SimulatedCard:Card_Data>" +
                "<emv.event:DestinationModule>CAL</emv.event:DestinationModule>" +
                "<emv.card:ProtocolLayerCDO>" +
                "<SignallingControlList>" +
                "<SignallingControl>ResumeRequest</SignallingControl>" +
                "</SignallingControlList>" +
                "<ResumeInterfaceID/>" +
                "<DisconnectIndicator>Empty</DisconnectIndicator>" +
                "</emv.card:ProtocolLayerCDO>" +
                "</emv.event.SimulatedCard:Card_Data>";
    }

    protected String card_data_4() {
        return "<emv.event.SimulatedCard:Card_Data>" +
                "<emv.event:DestinationModule>CAL</emv.event:DestinationModule>" +
                "<emv.card:MessageChoiceCDO>" +
                "<Extended>" +
                "<HeaderList>" +
                "<Header>ALPayload</Header>" +
                "</HeaderList>" +
                "<UnprotectedALPayloadContainer>" +
                "<emv.card:AuthCDO>" +
                "<Status>Online</Status>" +
                "<CardBlob>0102030405060708</CardBlob>" +
                "<ResponseRequirementCDO>" +
                "<Delegate>FullDelegation</Delegate>" +
                "<ReadyForOnlineResponse>False</ReadyForOnlineResponse>" +
                "</ResponseRequirementCDO>" +
                "</emv.card:AuthCDO>" +
                "<emv.card:NextCVMRequirementCDO>" +
                "<NextChoiceCDO>" +
                "<NextCVMCDO>" +
                "<MethodID>AnyOnDeviceCVM</MethodID>" +
                "</NextCVMCDO>" +
                "</NextChoiceCDO>" +
                "<CompletionStep>CVSuccessful</CompletionStep>" +
                "<CardPresent>False</CardPresent>" +
                "<ProcessedCDO>" +
                "<MethodID>AnyOnDeviceCVM</MethodID>" +
                "<Result>Successful</Result>" +
                "</ProcessedCDO>" +
                "</emv.card:NextCVMRequirementCDO>" +
                "</UnprotectedALPayloadContainer>" +
                "</Extended>" +
                "<Format>Extended</Format>" +
                "</emv.card:MessageChoiceCDO>" +
                "<emv.card:ProtocolLayerCDO>" +
                "<SignallingControlList>" +
                "<SignallingControl>DisconnectRequest</SignallingControl>" +
                "<SignallingControl>DeselectRequest</SignallingControl>" +
                "</SignallingControlList>" +
                "<ResumeInterfaceID/>" +
                "<DisconnectIndicator>Empty</DisconnectIndicator>" +
                "</emv.card:ProtocolLayerCDO>" +
                "</emv.event.SimulatedCard:Card_Data>";
    }
}

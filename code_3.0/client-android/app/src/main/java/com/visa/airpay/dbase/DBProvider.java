package com.visa.airpay.dbase;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class DBProvider extends ContentProvider {

    /**
     * How to use ADB to debug SQLite3:
     *      https://stackoverflow.com/questions/18370219/how-to-use-adb-in-android-studio-to-view-an-sqlite-db
     *
     *********************************************************************************************************/

    /**
     * Responsible to select which SQL operation to be executed
     */
    private static final UriMatcher mUriMatcher = buildUriMatcher();

    /**
     * SQLite data base helper
     */
    private DBHelper mDBHelper;

    /**
     * Integer used to execute query for the whole Trace table
     */
    private static final int TRANSACTION_ALL = 100;

    /**
     * Build Uri Matcher for query
     *
     * @return matcher object
     */
    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        String authority = DBContract.CONTENT_AUTHORITY;
        matcher.addURI(authority, DBContract.Trx.TABLE_NAME, TRANSACTION_ALL);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mDBHelper = DBHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor cursor;
        switch (mUriMatcher.match(uri)) {
            case TRANSACTION_ALL: {
                cursor = mDBHelper.getReadableDatabase().query(
                        DBContract.Trx.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            default: throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        Uri returnUri;
        switch (mUriMatcher.match(uri)) {
            case TRANSACTION_ALL: {
                long id = db.insert(DBContract.Trx.TABLE_NAME, null, contentValues);
                if (id > 0) returnUri = DBContract.Trx.buildUri(id);
                else throw new android.database.SQLException("Failed to insert row into: " + uri);
                break;
            }
            default: throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        int match = mUriMatcher.match(uri);
        int numDeleted;
        switch (match) {
            case TRANSACTION_ALL:
                numDeleted = db.delete(
                        DBContract.Trx.TABLE_NAME,
                        selection,
                        selectionArgs
                );
                db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + DBContract.Trx.TABLE_NAME + "'");
                break;
            default: throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return numDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}

package com.visa.airpay.bluetooth;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.visa.airpay.CvmActivity;
import com.visa.airpay.MainActivity;
import com.visa.airpay.R;
import com.visa.airpay.payment.AirpayApplet;
import com.visa.airpay.payment.CvmFlag;

public class BTService extends IntentService implements AirpayApplet.AirpayCallback {

    private static final String TAG = "BTService";

    public BTService() {
        super("BTService");
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, BTService.class);
        context.startService(intent);
    }

    public static void stop(Context context) {
        Intent intent = new Intent(context, BTService.class);
        context.stopService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AirpayApplet.getInstance().setOnCVMVerified(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent()");
        showNotification();
        BTManager.getInstance().connect();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        BTManager.getInstance().disconnect();
        AirpayApplet.getInstance().deleteInstance();
        CvmFlag.getInstance().destroyFlag();
        super.onDestroy();
    }

    @Override
    public void onCVMVerified(String amount) {

        Log.d(TAG, "onCVMVerified()");

        Intent intent = new Intent(this, CvmActivity.class);
        intent.putExtra("EXTRA_AMOUNT", amount);
        intent.putExtra("EXTRA_MERCHANT", "NTUC");
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);

        PendingIntent pIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new NotificationCompat.Builder(this, TAG)
                .setContentTitle(getText(R.string.notification_title))
                .setContentText("Pay SGD " + amount + " with airpay")
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentIntent(pIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setPriority(Notification.PRIORITY_HIGH)
                .setTicker(getText(R.string.ticker_text))
                .setAutoCancel(true)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(2,notification);
    }

    private void showNotification() {

        Intent intent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);

        PendingIntent pIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new NotificationCompat.Builder(this, TAG)
                        .setContentTitle(getText(R.string.notification_title))
                        .setContentText(getText(R.string.notification_message))
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentIntent(pIntent)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setTicker(getText(R.string.ticker_text))
                        .build();

        startForeground(1, notification);
    }

}

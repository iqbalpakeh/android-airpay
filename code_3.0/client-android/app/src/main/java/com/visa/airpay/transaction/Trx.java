package com.visa.airpay.transaction;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.visa.airpay.dbase.DBContract;

public class Trx {

    /**
     * Trx timestamp
     */
    private String mTimestamp;

    /**
     * Trx amount
     */
    private String mAmount;

    /**
     * Merchant where the transaction happened
     */
    private String mMerchant;

    /**
     * Build an object of transaction
     *
     * @param timestamp of transaction
     * @param amount of transaction
     * @param merchant where transaction happened
     * @return Trx object
     */
    public static Trx build(String timestamp, String amount, String merchant) {
        return new Trx(timestamp, amount, merchant);
    }

    private Trx(String timestamp, String amount, String merchant) {
        this.mTimestamp = timestamp;
        this.mAmount = amount;
        this.mMerchant = merchant;
    }

    /**
     * Build an object from cursor provided
     *
     * @param cursor from content provider
     * @return Trx object
     */
    public static Trx build(Cursor cursor) {
        String timestamp = cursor.getString(cursor.getColumnIndex(DBContract.Trx.COLUMN_TIMESTAMP));
        String amount = cursor.getString(cursor.getColumnIndex(DBContract.Trx.COLUMN_AMOUNT));
        String merchant = cursor.getString(cursor.getColumnIndex(DBContract.Trx.COLUMN_MERCHANT));
        return build(timestamp, amount, merchant);
    }

    /**
     * Get timestamp value
     *
     * @return timestamp
     */
    public String getTimestamp() {
        return mTimestamp;
    }

    /**
     * Get amount value
     *
     * @return amount
     */
    public String getAmount() {
        return mAmount;
    }

    /**
     * Get merchant name
     *
     * @return merchant
     */
    public String getMerchant() {
        return mMerchant;
    }

    /**
     * Store object to table on content provider
     *
     * @param context of application
     */
    public void store(Context context) {
        ContentValues values = new ContentValues();
        values.put(DBContract.Trx.COLUMN_TIMESTAMP, mTimestamp);
        values.put(DBContract.Trx.COLUMN_AMOUNT, mAmount);
        values.put(DBContract.Trx.COLUMN_MERCHANT, mMerchant);
        context.getContentResolver().insert(DBContract.Trx.CONTENT_URI, values);
    }

    /**
     * Clear all data from table
     *
     * @param context of application
     */
    public static void clearTable(Context context) {
        context.getContentResolver().delete(DBContract.Trx.CONTENT_URI, null, null);
    }

}

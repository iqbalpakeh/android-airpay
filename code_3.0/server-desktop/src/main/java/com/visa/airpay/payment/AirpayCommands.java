package com.visa.airpay.payment;

public class AirpayCommands {
    
    public String BeginTransaction(String amount, String date, String time) {
        return "<emv.event.POISystem:Begin_Transaction>"
            + "<emv.event:DestinationModule>CAL</emv.event:DestinationModule>"
            + "<emv.poi:POID qual=\"AvailableButEmpty\"/>"
            + "<emv.poi:Amount>" + amount +"</emv.poi:Amount>"
            + "<emv.poi:CurrencyCode>840</emv.poi:CurrencyCode>"
            + "<emv.poi:Date>"+ date +"</emv.poi:Date>"
            + "<emv.poi:Time>"+ time +"</emv.poi:Time>"
            + "<emv.poi:TransactionType>Payment</emv.poi:TransactionType>"
            + "<emv.poi:MerchantIndicatedHighRiskTransaction>False</emv.poi:MerchantIndicatedHighRiskTransaction>"
            + "<emv.poi:CountryCode>840</emv.poi:CountryCode>"
            + "<emv.poi:POIDIL qual=\"AvailableButEmpty\"/>"
            + "<emv.poi:FinalTransactionDataIndicator>True</emv.poi:FinalTransactionDataIndicator>"
            + "<emv.poi:AccountType>Default</emv.poi:AccountType>"
            + "</emv.event.POISystem:Begin_Transaction>";
    }

    public String ask_for_card(){
        return "<emv.event.CAL:Ask_For_Card>" +
                "<emv.event:DestinationModule>POISystem</emv.event:DestinationModule>" +
                "</emv.event.CAL:Ask_For_Card>";
    }

    public String kernel_open(String CCEID){
        // present card CCEID = NGSE
        // Processing card CCEID = A000000000B16F04
        // Completion CCEID = A000000000B16F04
        return "<emv.event.CAL:Kernel_Open>" +
                "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
                "<emv.scard:ActiveInterfaceID>Contactless</emv.scard:ActiveInterfaceID>" +
                "<emv.scard:CCEID>" + CCEID + "</emv.scard:CCEID>" +
                "</emv.event.CAL:Kernel_Open>";
    }

    public String kernel_close(String CCEID){
        // present card CCEID = NGSE
        // Processing card CCEID = A000000000B16F04
        return "<emv.event.CAL:Kernel_Close>" +
                "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
                "<emv.scard:CCEID>"+ CCEID + "</emv.scard:CCEID>" +
                "</emv.event.CAL:Kernel_Close>";
    }

    public String kernel_data_present_card(){
        return "<emv.event.CAL:Kernel_Data>" +
                "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
                "<emv.tx.dcm:MessageChoiceCDO>" +
                "<Extended>" +
                "<HeaderList>" +
                "<Header>ALDIL</Header>" +
                "</HeaderList>" +
                "<CDIL>" +
                "<DataID>emv.ngse:SvIDList</DataID>" +
                "<DataID>emv.ngse:ProposedAmount</DataID>" +
                "<DataID>emv.ngse:InterfaceIDList</DataID>" +
                "<DataID>emv.ngse:FormFactor</DataID>" +
                "</CDIL>" +
                "<UnprotectedALPayloadContainer/>" +
                "</Extended>" +
                "<Format>Extended</Format>" +
                "</emv.tx.dcm:MessageChoiceCDO>" +
                "</emv.event.CAL:Kernel_Data>";
    }

    public String kernel_data_processing(String amount, String date, String time){
        return  "<emv.event.CAL:Kernel_Data>" +
                "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
                "<emv.tx.dcm:MessageChoiceCDO>" +
                "<Extended>" +
                "<HeaderList>" +
                "<Header>ALDIL</Header>" +
                "<Header>ALPayload</Header>" +
                "</HeaderList>" +
                "<CDIL>" +
                "<DataID>emv.card:PAN</DataID>" +
                "<DataID>emv.card:ExpiryDate</DataID>" +
                "<DataID>emv.card:AuthCDO</DataID>" +
                "<DataID>emv.card:ConditionalAuthRequestDataIDList</DataID>" +
                "<DataID>emv.card:ConditionalTransactionCompletionDataIDList</DataID>" +
                "<DataID>emv.card:NextCVMRequirementCDO</DataID>" +
                "</CDIL>" +
                "<UnprotectedALPayloadContainer>" +
                "<emv.poi:Amount>"+ amount +"</emv.poi:Amount>" +
                "<emv.poi:CountryCode>840</emv.poi:CountryCode>" +
                "<emv.poi:CurrencyCode>840</emv.poi:CurrencyCode>" +
                "<emv.poi:Date>"+ date +"</emv.poi:Date>" +
                "<emv.poi:Time>"+ time +"</emv.poi:Time>" +
                "<emv.poi:TransactionType>Payment</emv.poi:TransactionType>" +
                "<emv.tx.tm:TerminalEnvironmentCDO>" +
                "<TerminalAttended>Attended</TerminalAttended>" +
                "<TerminalControl>Merchant</TerminalControl>" +
                "<TerminalLocation>Fixed</TerminalLocation>" +
                "<DisplayTypeList>" +
                "<DisplayType>Attendant</DisplayType>" +
                "<DisplayType>Cardholder</DisplayType>" +
                "<DisplayType>Merchant</DisplayType>" +
                "<DisplayType>Shared</DisplayType>" +
                "</DisplayTypeList>" +
                "<ReceiptProviderTypeList>" +
                "<ReceiptProviderType>Electronic</ReceiptProviderType>" +
                "<ReceiptProviderType>Printer</ReceiptProviderType>" +
                "</ReceiptProviderTypeList>" +
                "<PartialApprovalSupported>True</PartialApprovalSupported>" +
                "</emv.tx.tm:TerminalEnvironmentCDO>" +
                "<emv.tx.cvm:KernelSupportedCVMList>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>SignatureInline</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>SignatureAfterTransaction</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>AnyOnDeviceCVM</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>OutOfBandIDSolutionX</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>MOCPIN</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>MOIPIN</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>MOIPINWithCardKey</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>MOIPINEncryptedByCard</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>AnyOnDeviceMOICVM</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>MOCBiometricSolutionX</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>MOIBiometricSolutionX</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>MOIBiometricSolutionXWithCardKey</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "<KernelSupportedCVMCDO>" +
                "<MethodID>MOIBiometricSolutionXEncryptedByCardKey</MethodID>" +
                "<Status>Available</Status>" +
                "</KernelSupportedCVMCDO>" +
                "</emv.tx.cvm:KernelSupportedCVMList>" +
                "</UnprotectedALPayloadContainer>" +
                "</Extended>" +
                "<Format>Extended</Format>" +
                "</emv.tx.dcm:MessageChoiceCDO>" +
                "</emv.event.CAL:Kernel_Data>";
    }

    public String kernel_data_completion(){
        return "<emv.event.CAL:Kernel_Data>" +
                "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
                "<emv.tx.dcm:MessageChoiceCDO>" +
                "<Extended>" +
                "<HeaderList>" +
                "<Header>ALDIL</Header>" +
                "</HeaderList>" +
                "<CDIL>" +
                "<DataID>emv.card:AuthCDO</DataID>" +
                "<DataID>emv.card:NextCVMRequirementCDO</DataID>" +
                "</CDIL>" +
                "<UnprotectedALPayloadContainer/>" +
                "</Extended>" +
                "<Format>Extended</Format>" +
                "</emv.tx.dcm:MessageChoiceCDO>" +
                "</emv.event.CAL:Kernel_Data>";
    }

    public String kernel_signalling(String signal_control_list) {
        if (signal_control_list.equals("")){
            return "<emv.event.CAL:Kernel_Signaling>" +
                    "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
                    "<emv.scard:ProtocolLayerCDO>" +
                    "<SignallingControlList/>" +
                    "<ResumeInterfaceID/>" +
                    "<DisconnectIndicator>Empty</DisconnectIndicator>" +
                    "</emv.scard:ProtocolLayerCDO>" +
                    "</emv.event.CAL:Kernel_Signaling>";
        } else {
            return "<emv.event.CAL:Kernel_Signaling>" +
                    "<emv.event:DestinationModule>SimulatedCard</emv.event:DestinationModule>" +
                    "<emv.scard:ProtocolLayerCDO>" +
                    "<SignallingControlList>" +
                    "<SignallingControl>PositiveAcknowledgment</SignallingControl>" +
                    "</SignallingControlList>" +
                    "<ResumeInterfaceID/>" +
                    "<DisconnectIndicator>Empty</DisconnectIndicator>" +
                    "</emv.scard:ProtocolLayerCDO>" +
                    "</emv.event.CAL:Kernel_Signaling>";
        }
    }

    public String remove_card(){
        return "<emv.event.CAL:Remove_Card>" +
                "<emv.event:DestinationModule>POISystem</emv.event:DestinationModule>" +
                "</emv.event.CAL:Remove_Card>";
    }
            
}

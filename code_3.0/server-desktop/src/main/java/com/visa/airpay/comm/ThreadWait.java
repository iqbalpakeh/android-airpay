package com.visa.airpay.comm;

import com.intel.bluetooth.BlueCoveImpl;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import java.io.IOException;
import java.io.InterruptedIOException;

public class ThreadWait implements Runnable{

	private static final String AIRPAY_UUID = "04c6093b00001000800000805f9b34fd";

	public ThreadWait() {
	}
	
	@Override
	public void run() {
		waitForConnection();		
	}
	
	// Waiting for connection from devices
	private void waitForConnection() {
		
		StreamConnectionNotifier notifier;
		StreamConnection connection = null;
		
		// setup the server to listen for connection
		try {

			LocalDevice.getLocalDevice().setDiscoverable(DiscoveryAgent.GIAC);
			
			UUID uuid = new UUID(AIRPAY_UUID, false);
			System.out.println(uuid.toString());
			
            String url = "btspp://localhost:" + uuid.toString() + ";name=RemoteBluetooth";
			notifier = (StreamConnectionNotifier) Connector.open(url);
			
        } catch (BluetoothStateException e) {
        	System.out.println("Bluetooth is not turned on.");
			e.printStackTrace();
			return;

		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		// waiting for connection
		while(true) {

			try {

				System.out.println("waiting for connection...");
	            connection = notifier.acceptAndOpen();
	            
	            Thread processThread = new Thread(new ThreadConnection(connection));
	            processThread.start();
	            
			} catch (InterruptedIOException error) {
				BlueCoveImpl.shutdown();
				System.exit(0);	
			} catch (IOException error) {
				error.printStackTrace();
			}
		}
	}
}

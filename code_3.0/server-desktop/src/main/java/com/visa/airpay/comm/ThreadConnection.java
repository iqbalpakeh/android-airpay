package com.visa.airpay.comm;

import com.visa.airpay.payment.AirpayKernel;

import javax.microedition.io.StreamConnection;

public class ThreadConnection implements Runnable {

	public ThreadConnection(StreamConnection connection) {
		AirpayKernel.getInstance(connection);
	}
	
	@Override
	public void run() {
		AirpayKernel.getInstance().execute();
	}
}

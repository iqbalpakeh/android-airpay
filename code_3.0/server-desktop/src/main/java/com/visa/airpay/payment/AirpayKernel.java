package com.visa.airpay.payment;

import com.intel.bluetooth.BlueCoveImpl;
import com.visa.airpay.XMLParser;

import javax.microedition.io.StreamConnection;
import java.io.*;
import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class AirpayKernel {

    /**
     * Bluetooth connection object
     */
    private StreamConnection mConnection;

    /**
     * Static reference to AirpayKernel
     */
    private static AirpayKernel mKernel;

    /**
     * Get singleton instance of AirpayKernel
     *
     * @param connection of bluetooth
     * @return AirpayKernel object
     */
    public static synchronized AirpayKernel getInstance(StreamConnection connection) {
        if (mKernel == null) mKernel = new AirpayKernel(connection);
        return mKernel;
    }

    private AirpayKernel(StreamConnection connection) {
        mConnection = connection;
    }

    /**
     * Get singleton instance of AirpayKernel
     *
     * @return AirpayKernel object
     */
    public static synchronized AirpayKernel getInstance() {
        if (mKernel == null) mKernel = new AirpayKernel();
        return mKernel;
    }

    private AirpayKernel() {
        if (mConnection == null)
            throw new InvalidParameterException("Please call getInstance(StreamConnection connection)");
    }

    /**
     * Send all necessary command for one EMV transaction
     */
    public void execute() {

        Scanner input = new Scanner(System.in);
        AirpayCommands commands = new AirpayCommands();

        String command;
        String response;

        try {

            InputStream inputStream = mConnection.openInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            OutputStream out = mConnection.openOutputStream();
            PrintStream printer = new PrintStream(out);

            while (true) {

                System.out.println("\nProvide Transaction Amount:");
                String value = input.nextLine();
                String str_date;
                String str_time;
                String str_xml;

                if (value.equals("q")) {
                    mConnection.close();
                    BlueCoveImpl.shutdown();
                    System.exit(0);
                }

                System.out.println("\nTransaction Amount = " + value + " USD\n");

                // Get current tdate time
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                String datetime = dtf.format(now);

                // split into date and time
                String[] parts = datetime.split(" ");
                str_date = parts[0];
                str_time = parts[1];

                str_xml = commands.BeginTransaction(value, str_date, str_time);
                if (str_xml == "") {
                    System.out.println("Error in POI Begin Transsaction !");
                    break; // error
                }
                System.out.println(XMLParser.print(str_xml));

                str_xml = commands.ask_for_card();
                // call gui ?
                System.out.println(XMLParser.print(str_xml));

                command = commands.kernel_open("NGSE") + "\n";
                ;
                printer.print(command);
                while ((response = reader.readLine()) != null) {
                    System.out.print("Command = " + XMLParser.print(command));
                    System.out.println("Response = " + XMLParser.print(response) + "\n");
                    break;
                }

                command = commands.kernel_data_present_card() + "\n";
                printer.print(command);
                while ((response = reader.readLine()) != null) {
                    System.out.print("Command = " + XMLParser.print(command));
                    System.out.println("Response = " + XMLParser.print(response) + "\n");
                    break;
                }

                command = commands.kernel_close("NGSE") + "\n";
                printer.print(command);
                while ((response = reader.readLine()) != null) {
                    System.out.print("Command = " + XMLParser.print(command));
                    System.out.println("Response = " + XMLParser.print(response) + "\n");
                    break;
                }

                // Kernel open 2nd time for CDCVM
                command = commands.kernel_open("A000000000B16F04") + "\n";
                printer.print(command);
                while ((response = reader.readLine()) != null) {
                    System.out.print("Command = " + XMLParser.print(command));
                    System.out.println("Response = " + XMLParser.print(response) + "\n");
                    break;
                }

                command = commands.kernel_data_processing(value, str_date, str_time) + "\n";
                printer.print(command);
                while ((response = reader.readLine()) != null) {
                    System.out.print("Command = " + XMLParser.print(command));
                    System.out.println("Response = " + XMLParser.print(response) + "\n");
                    break;
                }

                // after this, the card/phone should prompt user for ack and wait for command again?
                command = commands.kernel_signalling("positive_acknowledgement") + "\n";
                printer.print(command);
                while ((response = reader.readLine()) != null) {
                    System.out.print("Command = " + XMLParser.print(command));
                    System.out.println("Response = " + XMLParser.print(response) + "\n");
                    break;
                }

                str_xml = commands.remove_card();
                System.out.println(XMLParser.print(str_xml));
                // need to inform POS ? ?

                System.out.println("==== Waiting for CDCVM ====\n");

                str_xml = commands.ask_for_card();
                System.out.println(XMLParser.print(str_xml));

                // Kernel open 3rd time for completion
                command = commands.kernel_open("A000000000B16F04") + "\n";
                printer.print(command);
                while ((response = reader.readLine()) != null) {
                    System.out.print("Command = " + XMLParser.print(command));
                    System.out.println("Response = " + XMLParser.print(response) + "\n");
                    break;
                }

                command = commands.kernel_signalling("") + "\n";
                printer.print(command);
                while ((response = reader.readLine()) != null) {
                    System.out.print("Command = " + XMLParser.print(command));
                    System.out.println("Response = " + XMLParser.print(response) + "\n");
                    break;
                }

                command = commands.kernel_signalling("positive_acknowledgement") + "\n";
                printer.print(command);
                while ((response = reader.readLine()) != null) {
                    System.out.print("Command = " + XMLParser.print(command));
                    System.out.println("Response = " + XMLParser.print(response) + "\n");
                    break;
                }

                command = commands.kernel_data_completion() + "\n";
                printer.print(command);
                while ((response = reader.readLine()) != null) {
                    System.out.print("Command = " + XMLParser.print(command));
                    System.out.println("Response = " + XMLParser.print(response) + "\n");
                    break;
                }

                System.out.println("==== Transaction Completed ====\n");
                // Transaction Complete

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            input.close();
        }
    }

}

package com.visa.airpay;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class Util {

    /**
     * For easy debug log activation. Value must be false for release version
     */
    private static final boolean DEBUG = true;

    /**
     * Function to log important message to help debugging. Make sure this function comment out
     * before releasing apk
     *
     * @param tag debug filter
     * @param message debug message
     */
    public static void Log(String tag, String message) {
        if (DEBUG) Log.d(tag, message);
    }

    /**
     * Function to log important error message to help debugging. Make sure this function comment out
     * before releasing apk
     *
     * @param tag debug filter
     * @param message debug message
     * @param tr throwable object
     */
    public static void Error(String tag, String message, Throwable tr) {
        if (DEBUG) Log.e(tag, message, tr);
    }

    /**
     * Warn user with toast message
     *
     * @param context of application
     * @param message for user
     */
    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}

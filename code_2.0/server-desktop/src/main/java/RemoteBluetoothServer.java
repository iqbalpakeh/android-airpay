public class RemoteBluetoothServer {

	// IMPORTANT:
	//
	// 1. Sample code between java desktop and android
	//    http://luugiathuy.com/2011/02/android-java-bluetooth/
	// 
	// 2. Working BlueCode version on mac
	//    https://stackoverflow.com/questions/22892738/mac-os-and-java-bluetooth
	//    https://code.google.com/archive/p/bluecove/issues/134
	//
	// 3. Someone fork the repository from bluecove.org and maintain 
	//    on github (not maintain by the group anymore??)
	//    https://github.com/machaval/bluecove
	//
	//4. Official Java Api from Oracle
	//   http://www.oracle.com/technetwork/articles/javame/index-156193.html
	//
	public static void main(String[] args) {
		Thread waitThread = new Thread(new WaitThread());
		waitThread.start();
	}
}
